<?php

namespace Garbee\MediaLibrary\UrlGenerator;

use Garbee\MediaLibrary\Conversion\Conversion;
use Garbee\MediaLibrary\Media;
use Garbee\MediaLibrary\PathGenerator\PathGenerator;

interface UrlGenerator
{
    /**
     * Get the url for the profile of a media item.
     *
     * @return string
     */
    public function getUrl() : string;

    /**
     * @param \Garbee\MediaLibrary\Media $media
     *
     * @return \Garbee\MediaLibrary\UrlGenerator\UrlGenerator
     */
    public function setMedia(Media $media) : UrlGenerator;

    /**
     * @param \Garbee\MediaLibrary\Conversion\Conversion $conversion
     *
     * @return \Garbee\MediaLibrary\UrlGenerator\UrlGenerator
     */
    public function setConversion(Conversion $conversion) : UrlGenerator;

    /**
     * Set the path generator class.
     *
     * @param \Garbee\MediaLibrary\PathGenerator\PathGenerator $pathGenerator
     *
     * @return \Garbee\MediaLibrary\UrlGenerator\UrlGenerator
     */
    public function setPathGenerator(PathGenerator $pathGenerator) : UrlGenerator;
}
