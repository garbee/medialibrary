<?php

namespace Garbee\MediaLibrary\UrlGenerator;

use Illuminate\Contracts\Config\Repository as Config;
use Garbee\MediaLibrary\Conversion\Conversion;
use Garbee\MediaLibrary\Media;
use Garbee\MediaLibrary\PathGenerator\PathGenerator;

abstract class BaseUrlGenerator implements UrlGenerator
{
    /**
     * @var \Garbee\MediaLibrary\Media
     */
    protected $media;

    /**
     * @var \Garbee\MediaLibrary\Conversion\Conversion
     */
    protected $conversion;

    /**
     * @var \Garbee\MediaLibrary\PathGenerator\PathGenerator
     */
    protected $pathGenerator;

    /**
     * @var \Illuminate\Contracts\Config\Repository
     */
    protected $config;

    /**
     * @param \Illuminate\Contracts\Config\Repository $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @param \Garbee\MediaLibrary\Media $media
     *
     * @return \Garbee\MediaLibrary\UrlGenerator\UrlGenerator
     */
    public function setMedia(Media $media) : UrlGenerator
    {
        $this->media = $media;

        return $this;
    }

    /**
     * @param \Garbee\MediaLibrary\Conversion\Conversion $conversion
     *
     * @return \Garbee\MediaLibrary\UrlGenerator\UrlGenerator
     */
    public function setConversion(Conversion $conversion) : UrlGenerator
    {
        $this->conversion = $conversion;

        return $this;
    }

    /**
     * @param \Garbee\MediaLibrary\PathGenerator\PathGenerator $pathGenerator
     *
     * @return \Garbee\MediaLibrary\UrlGenerator\UrlGenerator
     */
    public function setPathGenerator(PathGenerator $pathGenerator) : UrlGenerator
    {
        $this->pathGenerator = $pathGenerator;

        return $this;
    }

    /*
     * Get the path to the requested file relative to the root of the media directory.
     */
    public function getPathRelativeToRoot() : string
    {
        if (is_null($this->conversion)) {
            return $this->pathGenerator->getPath($this->media).$this->media->file_name;
        }

        return $this->pathGenerator->getPathForConversions($this->media)
        .$this->conversion->getName()
        .'.'
        .$this->conversion->getResultExtension($this->media->extension);
    }
}
