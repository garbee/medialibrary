<?php

namespace Garbee\MediaLibrary\Jobs;

use Illuminate\Bus\Queueable;

abstract class Job
{
    use Queueable;
}
