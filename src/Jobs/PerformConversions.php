<?php

namespace Garbee\MediaLibrary\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Garbee\MediaLibrary\Conversion\ConversionCollection;
use Garbee\MediaLibrary\FileManipulator;
use Garbee\MediaLibrary\Media;

class PerformConversions extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var \Garbee\MediaLibrary\Conversion\ConversionCollection
     */
    protected $conversions;

    /**
     * @var \Garbee\MediaLibrary\Media
     */
    protected $media;

    public function __construct(ConversionCollection $conversions, Media $media)
    {
        $this->conversions = $conversions;
        $this->media = $media;
    }

    public function handle() : bool
    {
        app(FileManipulator::class)->performConversions($this->conversions, $this->media);

        return true;
    }
}
