<?php

namespace Garbee\MediaLibrary\Events;

use Illuminate\Queue\SerializesModels;
use Garbee\MediaLibrary\Media;

class MediaHasBeenAdded
{
    use SerializesModels;

    /**
     * @var \Garbee\MediaLibrary\Media
     */
    public $media;

    /*
     * @param \Garbee\MediaLibrary\Media $media
     */
    public function __construct(Media $media)
    {
        $this->media = $media;
    }
}
