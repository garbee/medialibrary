<?php

namespace Garbee\MediaLibrary\Events;

use Illuminate\Queue\SerializesModels;
use Garbee\MediaLibrary\Conversion\Conversion;
use Garbee\MediaLibrary\Media;

class ConversionHasBeenCompleted
{
    use SerializesModels;

    /**
     * @var \Garbee\MediaLibrary\Media
     */
    public $media;

    /**
     * @var \Garbee\MediaLibrary\Conversion\Conversion
     */
    public $conversion;

    /**
     * ConversionHasFinishedEvent constructor.
     *
     * @param \Garbee\MediaLibrary\Media                 $media
     * @param \Garbee\MediaLibrary\Conversion\Conversion $conversion
     */
    public function __construct(Media $media, Conversion $conversion)
    {
        $this->media = $media;
        $this->conversion = $conversion;
    }
}
