<?php

namespace Garbee\MediaLibrary\Events;

use Illuminate\Queue\SerializesModels;
use Garbee\MediaLibrary\HasMedia\Interfaces\HasMedia;

class CollectionHasBeenCleared
{
    use SerializesModels;

    /**
     * @var \Garbee\MediaLibrary\HasMedia\Interfaces\HasMedia
     */
    public $model;

    /**
     * @var string
     */
    public $collectionName;

    public function __construct(HasMedia $model, string $collectionName)
    {
        $this->model = $model;
        $this->collectionName = $collectionName;
    }
}
