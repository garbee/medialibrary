<?php

namespace Garbee\MediaLibrary\Conversion;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Relations\Relation;
use Garbee\MediaLibrary\Exceptions\InvalidConversion;
use Garbee\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Garbee\MediaLibrary\Media;

class ConversionCollection extends Collection
{
    /**
     * @param \Garbee\MediaLibrary\Media $media
     *
     * @return static
     */
    public static function createForMedia(Media $media)
    {
        return (new static())->setMedia($media);
    }

    /**
     * @param \Garbee\MediaLibrary\Media $media
     *
     * @return $this
     */
    public function setMedia(Media $media)
    {
        $this->items = [];

        $this->addConversionsFromRelatedModel($media);

        $this->addManipulationsFromDb($media);

        return $this;
    }

    /**
     *  Get a conversion by it's name.
     *
     * @param string $name
     *
     * @return mixed
     *
     * @throws \Garbee\MediaLibrary\Exceptions\InvalidConversion
     */
    public function getByName(string $name)
    {
        $conversions = collect($this->items)->filter(function(Conversion $conversion) use ($name) {
            return $conversion->getName() === $name;
        });

        if ($conversions->isEmpty()) {
            throw InvalidConversion::unknownName($name);
        }

        return $conversions->first();
    }

    /**
     * Add the conversion that are defined on the related model of
     * the given media.
     *
     * @param \Garbee\MediaLibrary\Media $media
     */
    protected function addConversionsFromRelatedModel(Media $media)
    {
        $modelName = Arr::get(Relation::morphMap(), $media->model_type, $media->model_type);

        /*
         * To prevent an sql query create a new model instead
         * of the using the associated one
         */
        $model = new $modelName();

        if ($model instanceof HasMediaConversions) {
            $model->registerMediaConversions();
        }

        $this->items = $model->mediaConversions;
    }

    /**
     * Add the extra manipulations that are defined on the given media.
     *
     * @param \Garbee\MediaLibrary\Media $media
     */
    protected function addManipulationsFromDb(Media $media)
    {
        foreach ($media->manipulations as $conversionName => $manipulation) {
            $this->addManipulationToConversion($manipulation, $conversionName);
        }
    }

    /**
     * Get all the conversions in the collection.
     *
     * @param string $collectionName
     *
     * @return $this
     */
    public function getConversions(string $collectionName = '')
    {
        if ($collectionName === '') {
            return $this;
        }

        return $this->filter(function (Conversion $conversion) use ($collectionName) {
            return $conversion->shouldBePerformedOn($collectionName);
        });
    }

    /*
     * Get all the conversions in the collection that should be queued.
     */
    public function getQueuedConversions(string $collectionName = '') : ConversionCollection
    {
        return $this->getConversions($collectionName)->filter(function (Conversion $conversion) {
            return $conversion->shouldBeQueued();
        });
    }

    /*
     * Add the given manipulation to the conversion with the given name.
     */
    protected function addManipulationToConversion(array $manipulation, string $conversionName)
    {
        foreach ($this as $conversion) {
            if ($conversion->getName() === $conversionName) {
                $conversion->addAsFirstManipulation($manipulation);

                return;
            }
        }
    }

    /*
     * Get all the conversions in the collection that should not be queued.
     */
    public function getNonQueuedConversions(string $collectionName = '') : ConversionCollection
    {
        return $this->getConversions($collectionName)->filter(function (Conversion $conversion) {
            return !$conversion->shouldBeQueued();
        });
    }
}
