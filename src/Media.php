<?php

namespace Garbee\MediaLibrary;

use Illuminate\Database\Eloquent\Model;
use Garbee\MediaLibrary\Conversion\Conversion;
use Garbee\MediaLibrary\Conversion\ConversionCollection;
use Garbee\MediaLibrary\Helpers\File;
use Garbee\MediaLibrary\UrlGenerator\UrlGeneratorFactory;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * Class Media
 * @package Garbee\MediaLibrary
 *
 * @property string $extension
 */
class Media extends Model
{
    use SortableTrait;

    const TYPE_OTHER = 'other';
    const TYPE_IMAGE = 'image';
    const TYPE_PDF = 'pdf';

    protected $guarded = ['id', 'disk', 'file_name', 'size', 'model_type', 'model_id'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'manipulations' => 'array',
        'custom_properties' => 'array',
    ];

    /**
     * Create the polymorphic relation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function model(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * Get the original Url to a media-file.
     *
     * @param string $conversionName
     *
     * @return string
     *
     * @throws \Garbee\MediaLibrary\Exceptions\InvalidConversion
     */
    public function getUrl(string $conversionName = '') : string
    {
        $urlGenerator = UrlGeneratorFactory::createForMedia($this);

        if ($conversionName !== '') {
            $urlGenerator->setConversion(
                ConversionCollection::createForMedia($this)
                    ->getByName($conversionName)
            );
        }

        return $urlGenerator->getUrl();
    }

    /**
     * Get the original path to a media-file.
     *
     * @param string $conversionName
     *
     * @return string
     *
     * @throws \Garbee\MediaLibrary\Exceptions\InvalidConversion
     */
    public function getPath(string $conversionName = '') : string
    {
        $urlGenerator = UrlGeneratorFactory::createForMedia($this);

        if ($conversionName !== '') {
            $urlGenerator->setConversion(
                ConversionCollection::createForMedia($this)
                    ->getByName($conversionName)
            );
        }

        return $urlGenerator->getPath();
    }

    /**
     * Determine the type of a file.
     *
     * @return string
     */
    public function getTypeAttribute()
    {
        $type = $this->type_from_extension;
        if ($type !== self::TYPE_OTHER) {
            return $type;
        }

        return $this->type_from_mime;
    }

    /**
     * Determine the type of a file from its file extension.
     *
     * @return string
     */
    public function getTypeFromExtensionAttribute(): string
    {
        switch (strtolower($this->extension)) {
            case 'png':
            case 'jpg':
            case 'jpeg':
            case 'gif':
                return static::TYPE_IMAGE;
            break;
            case 'pdf':
                return static::TYPE_PDF;
            break;
            default:
                return static::TYPE_OTHER;
        }
    }

    /*
     * Determine the type of a file from its mime type
     */
    public function getTypeFromMimeAttribute() : string
    {
        if ($this->getDiskDriverName() !== 'local') {
            return static::TYPE_OTHER;
        }

        switch (File::getMimetype($this->getPath())) {
            case 'image/jpeg':
            case 'image/gif':
            case 'image/png':
                return static::TYPE_IMAGE;
            break;
            case 'application/pdf':
                return static::TYPE_PDF;
            break;
            default:
                return static::TYPE_OTHER;
        }
    }

    public function getExtensionAttribute() : string
    {
        return pathinfo($this->file_name, PATHINFO_EXTENSION);
    }

    public function getHumanReadableSizeAttribute() : string
    {
        return File::getHumanReadableSize($this->size);
    }

    public function getDiskDriverName() : string
    {
        return config("filesystems.disks.{$this->disk}.driver");
    }

    /*
     * Determine if the media item has a custom property with the given name.
     */
    public function hasCustomProperty(string $propertyName) : bool
    {
        return array_key_exists($propertyName, $this->custom_properties);
    }

    /**
     * Get if the value of custom property with the given name.
     *
     * @param string $propertyName
     * @param mixed  $default
     *
     * @return mixed
     */
    public function getCustomProperty(string $propertyName, $default = null)
    {
        return $this->custom_properties[$propertyName] ?? $default;
    }

    /**
     * @param string $name
     * @param mixed  $value
     */
    public function setCustomProperty(string $name, $value)
    {
        $this->custom_properties = array_merge($this->custom_properties, [$name => $value]);
    }

    /*
     * Get all the names of the registered media conversions.
     */
    public function getMediaConversionNames() : array
    {
        $conversions = ConversionCollection::createForMedia($this);

        return $conversions->map(function (Conversion $conversion) {
            return $conversion->getName();
        })->toArray();
    }
}
