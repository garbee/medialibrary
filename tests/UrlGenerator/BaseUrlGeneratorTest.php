<?php

namespace Garbee\MediaLibrary\Test\UrlGenerator;

use Garbee\MediaLibrary\Conversion\ConversionCollection;
use Garbee\MediaLibrary\PathGenerator\BasePathGenerator;
use Garbee\MediaLibrary\Test\TestCase;
use Garbee\MediaLibrary\UrlGenerator\LocalUrlGenerator;

class BaseUrlGeneratorTest extends TestCase
{
    protected $config;

    /**
     * @var \Garbee\MediaLibrary\Media
     */
    protected $media;

    /**
     * @var \Garbee\MediaLibrary\Conversion\Conversion
     */
    protected $conversion;

    /**
     * @var LocalUrlGenerator
     */
    protected $urlGenerator;

    /**
     * @var BasePathGenerator
     */
    protected $pathGenerator;

    public function setUp()
    {
        parent::setUp();

        $this->config = app('config');

        $this->media = $this->testModelWithConversion->addMedia($this->getTestFilesDirectory('test.jpg'))->toMediaLibrary();

        $this->conversion = ConversionCollection::createForMedia($this->media)->getByName('thumb');

        // because BaseUrlGenerator is abstract we'll use LocalUrlGenerator to test the methods of base
        $this->urlGenerator = new LocalUrlGenerator($this->config);
        $this->pathGenerator = new BasePathGenerator();

        $this->urlGenerator
            ->setMedia($this->media)
            ->setConversion($this->conversion)
            ->setPathGenerator($this->pathGenerator);
    }

    /** @test */
    public function it_can_get_the_path_relative_to_the_root_of_media_folder()
    {
        $pathRelativeToRoot = $this->media->id.'/conversions/'.$this->conversion->getName().'.'.$this->conversion->getResultExtension($this->media->extension);

        $this->assertEquals($pathRelativeToRoot, $this->urlGenerator->getPathRelativeToRoot());
    }
}
