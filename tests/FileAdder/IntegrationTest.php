<?php

namespace Garbee\MediaLibrary\Test\FileAdder;

use Garbee\MediaLibrary\Exceptions\FileCannotBeAdded;
use Garbee\MediaLibrary\Media;
use Garbee\MediaLibrary\Test\TestCase;
use Garbee\MediaLibrary\Test\TestModel;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class IntegrationTest extends TestCase
{
    /** @test */
    public function it_can_add_an_file_to_the_default_collection()
    {
        $media = $this->testModel
            ->addMedia($this->getTestJpg())
            ->toMediaLibrary();

        self::assertEquals('default', $media->collection_name);
    }

    /** @test */
    public function it_will_throw_an_exception_when_adding_a_non_existing_file()
    {
        $this->expectException(FileCannotBeAdded::class);

        (new TestModel())
            ->addMedia($this->getTestJpg())
            ->toMediaLibrary();
    }

    /** @test */
    public function it_will_throw_an_exception_when_adding_a_non_saved_model()
    {
        $this->expectException(FileCannotBeAdded::class);

        $this->testModel
            ->addMedia('/home/blablaba')
            ->toMediaLibrary();
    }

    /** @test */
    public function it_can_set_the_name_of_the_media()
    {
        $media = $this->testModel
            ->addMedia($this->getTestJpg())
            ->toMediaLibrary();

        self::assertEquals('test', $media->name);
    }

    /** @test */
    public function it_can_add_a_file_to_a_named_collection()
    {
        $collectionName = 'images';

        $media = $this->testModel->addMedia($this->getTestJpg())->toCollection($collectionName);

        self::assertEquals($collectionName, $media->collection_name);
    }

    /** @test */
    public function it_can_move_the_original_file_to_the_medialibrary()
    {
        $testFile = $this->getTestJpg();

        $media = $this->testModel
            ->addMedia($testFile)
            ->toMediaLibrary();

        self::assertFileNotExists($testFile);
        self::assertFileExists($this->getMediaDirectory("{$media->id}/{$media->file_name}"));
    }

    /** @test */
    public function it_can_copy_the_original_file_to_the_medialibrary()
    {
        $testFile = $this->getTestJpg();

        $media = $this->testModel->copyMedia($testFile)->toCollection('images');

        self::assertFileExists($testFile);
        self::assertFileExists($this->getMediaDirectory($media->id.'/'.$media->file_name));
    }

    /** @test */
    public function it_can_handle_a_file_without_an_extension()
    {
        $media = $this->testModel->addMedia($this->getTestFilesDirectory('test'))->toMediaLibrary();

        self::assertEquals('test', $media->name);

        self::assertEquals('test', $media->file_name);

        self::assertEquals("/media/{$media->id}/test", $media->getUrl());

        self::assertFileExists($this->getMediaDirectory("/{$media->id}/test"));
    }

    /** @test */
    public function it_can_handle_an_image_file_without_an_extension()
    {
        $media = $this->testModel->addMedia($this->getTestFilesDirectory('image'))->toMediaLibrary();

        self::assertEquals(Media::TYPE_IMAGE, $media->type);
    }

    /** @test */
    public function it_can_handle_a_non_image_and_non_pdf_file()
    {
        $media = $this->testModel->addMedia($this->getTestFilesDirectory('test.txt'))->toMediaLibrary();

        self::assertEquals('test', $media->name);

        self::assertEquals('test.txt', $media->file_name);

        self::assertEquals("/media/{$media->id}/test.txt", $media->getUrl());

        self::assertFileExists($this->getMediaDirectory("/{$media->id}/test.txt"));
    }

    /** @test */
    public function it_can_add_an_upload_to_the_medialibrary()
    {
        $uploadedFile = new UploadedFile(
            $this->getTestFilesDirectory('test.jpg'),
            'alternativename.jpg',
            'image/jpeg',
            filesize($this->getTestFilesDirectory('test.jpg'))
        );

        $media = $this->testModel->addMedia($uploadedFile)->toMediaLibrary();
        self::assertEquals('alternativename', $media->name);
        self::assertFileExists($this->getMediaDirectory($media->id.'/'.$media->file_name));
    }

    /** @test */
    public function it_can_add_an_upload_to_the_medialibrary_from_the_current_request()
    {
        $this->app['router']->get('/upload', function () {
            $media = $this->testModel->addMediaFromRequest('file')->toMediaLibrary();
            self::assertEquals('alternativename', $media->name);
            self::assertFileExists($this->getMediaDirectory("{$media->id}/{$media->file_name}"));
        });

        $fileUpload = new UploadedFile(
            $this->getTestFilesDirectory('test.jpg'),
            'alternativename.jpg',
            'image/jpeg',
            filesize($this->getTestFilesDirectory('test.jpg'))
        );

        $this->call('get', 'upload', [], [], ['file' => $fileUpload]);
    }

    /** @test */
    public function it_will_throw_an_exception_when_trying_to_add_a_non_existing_key_from_a_request()
    {
        $this->app['router']->get('/upload', function () {
            $exceptionWasThrown = false;
            try {
                $this->testModel->addMediaFromRequest('non existing key')->toMediaLibrary();
            } catch (FileCannotBeAdded $exception) {
                $exceptionWasThrown = true;
            }
            self::assertTrue($exceptionWasThrown);
        });
        $this->call('get', 'upload');
    }

    /** @test */
    public function it_can_add_a_remote_file_to_the_medialibrary()
    {
        $url = 'https://docs.spatie.be/images/medialibrary/header.jpg';

        $media = $this->testModel
            ->addMediaFromUrl($url)
            ->toMediaLibrary();

        self::assertEquals('header', $media->name);
        self::assertFileExists($this->getMediaDirectory("{$media->id}/header.jpg"));
    }

    /** @test */
    public function it_wil_thrown_an_exception_when_a_remote_file_could_not_be_added()
    {
        $url = 'https://docs.spatie.be/images/medialibrary/thisonedoesnotexist.jpg';

        $this->expectException(FileCannotBeAdded::class);

        $this->testModel
            ->addMediaFromUrl($url)
            ->toMediaLibrary();
    }

    /** @test */
    public function it_can_rename_the_media_before_it_gets_added()
    {
        $media = $this->testModel
            ->addMedia($this->getTestJpg())
            ->usingName('othername')
            ->toMediaLibrary();

        self::assertEquals('othername', $media->name);
        self::assertFileExists($this->getMediaDirectory($media->id.'/test.jpg'));
    }

    /** @test */
    public function it_can_rename_the_file_before_it_gets_added()
    {
        $media = $this->testModel
            ->addMedia($this->getTestJpg())
            ->usingFileName('othertest.jpg')
            ->toMediaLibrary();

        self::assertEquals('test', $media->name);
        self::assertFileExists($this->getMediaDirectory($media->id.'/othertest.jpg'));
    }

    /** @test */
    public function it_will_sanitize_the_file_name()
    {
        $media = $this->testModel
            ->addMedia($this->getTestJpg())
            ->usingFileName('other#test.jpg')
            ->toMediaLibrary();

        self::assertEquals('test', $media->name);
        self::assertFileExists($this->getMediaDirectory($media->id.'/other-test.jpg'));
    }

    /** @test */
    public function it_can_save_media_in_the_right_order()
    {
        $media = [];
        foreach (range(0, 5) as $index) {
            $media[] = $this->testModel
                ->addMedia($this->getTestJpg())
                ->preservingOriginal()
                ->toMediaLibrary();

            self::assertEquals($index + 1, $media[$index]->order_column);
        }
    }

    /** @test */
    public function it_can_add_properties_to_the_saved_media()
    {
        $media = $this->testModel
            ->addMedia($this->getTestJpg())
            ->preservingOriginal()
            ->withProperties(['name' => 'testName'])
            ->toMediaLibrary();

        self::assertEquals('testName', $media->name);

        $media = $this->testModel
            ->addMedia($this->getTestJpg())
            ->preservingOriginal()
            ->withAttributes(['name' => 'testName'])
            ->toMediaLibrary();

        self::assertEquals('testName', $media->name);
    }

    /** @test */
    public function it_can_add_file_to_model_with_morph_map()
    {
        $media = $this->testModelWithMorphMap
            ->addMedia($this->getTestJpg())
            ->toMediaLibrary();

        self::assertEquals($this->testModelWithMorphMap->getMorphClass(), $media->model_type);
    }
}
