<?php

namespace Garbee\MediaLibrary\Test;

use Illuminate\Database\Eloquent\Model;
use Garbee\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Garbee\MediaLibrary\HasMedia\HasMediaTrait;

class TestModelWithoutMediaConversions extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $table = 'test_models';
    protected $guarded = [];
    public $timestamps = false;
}
