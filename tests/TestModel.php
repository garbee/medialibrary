<?php

namespace Garbee\MediaLibrary\Test;

use Illuminate\Database\Eloquent\Model;
use Garbee\MediaLibrary\HasMedia\HasMediaTrait;
use Garbee\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;

class TestModel extends Model implements HasMediaConversions
{
    use HasMediaTrait;

    protected $table = 'test_models';
    protected $guarded = [];
    public $timestamps = false;

    /**
     * Register the conversions that should be performed.
     *
     * @return array
     */
    public function registerMediaConversions()
    {
    }
}
