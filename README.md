# Media Library

This is a fork of Spatie's laravel-medialibrary package.

Current differences are:

* Command provided to check for file existence on the drive.
* Ability to customize the queue class via the config file.

The current plan is to slowly rework the internal code to be engineered differently while maintaining the existing public API.

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
